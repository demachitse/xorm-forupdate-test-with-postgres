package main

import (
	"fmt"
	"log"
	"os"

	"github.com/lib/pq"
	"xorm.io/builder"
	"xorm.io/xorm"
	xLog "xorm.io/xorm/log"

	"gitea.com/demachitse/xorm-forupdate-test-with-postgres/db_model"
)

func main() {
	userId := "postgres"
	password := "postgres"
	dbName := "postgres"
	dbHost := "localhost"
	dbPort := "5432"

	dsn := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", userId, password, dbName, dbHost, dbPort)

	db, err := xorm.NewEngine("postgres", dsn)
	if err != nil {
		log.Fatalf("failed to create db instance: %v", err)
	}

	logger := xLog.NewSimpleLogger(os.Stdout)
	db.SetLogger(logger)
	db.ShowSQL(true)

	selectAndUpdateWithForUpdate(db)
}

func selectAndUpdateWithForUpdate(db *xorm.Engine) {
	session := db.NewSession()
	session.Begin()
	if err := session.Begin(); err != nil {
		log.Fatalf("failed to begin transaction: %v", err)
	}
	defer session.Close()

	results := []db_model.DummyTableA{}
	cond := builder.NewCond()
	cond = cond.Or(builder.Eq{"id": "xid000000000000000a1"})
	err := session.ForUpdate().Where(cond).Find(&results)
	if err != nil {
		log.Fatalf("failed to find dummy_table_a: %v", err)
	}

	recordForUpdate := db_model.DummyTableB{
		Value: "updatedValue",
	}
	cond = builder.NewCond()
	cond = cond.Or(builder.Eq{"id": "xid000000000000000b1"})
	_, err = session.Where(cond).Update(recordForUpdate)
	if err != nil {
		switch err := err.(type) {
		case *pq.Error:
			switch err.Code{
			case "40P01":
				log.Fatalf("deadlock detected in update dummy_table_b: %v", err)
			default:
				log.Fatalf("failed to update dummy_table_b: %v", err)
		}
		default:
			log.Fatalf("failed to update dummy_table_b: %v", err)
		}
	}

	err = session.Commit()
	if err != nil {
		session.Rollback()
		log.Fatalf("[selectAndUpdateWithForUpdate]: failed to commit: %v", err)
	}
}

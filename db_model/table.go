package db_model

import "time"

type DummyTableA struct {
	Id        string    `xorm:"id"`
	Name      string    `xorm:"name"`
	Value     string    `xorm:"value"`
	CreatedAt time.Time `xorm:"created_at"`
	UpdatedAt time.Time `xorm:"updated_at"`
}

func (DummyTableA) TableName() string {
	return "dummy_table_a"
}

type DummyTableB struct {
	Id         string    `xorm:"id"`
	Name       string    `xorm:"name"`
	Value      string    `xorm:"value"`
	AttributeA *string   `xorm:"attribute_a"`
	AttributeB *string   `xorm:"attribute_b"`
	CreatedAt  time.Time `xorm:"created_at"`
	UpdatedAt  time.Time `xorm:"updated_at"`
}

func (DummyTableB) TableName() string {
	return "dummy_table_b"
}

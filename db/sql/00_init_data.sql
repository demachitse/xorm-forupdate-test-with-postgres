CREATE TABLE dummy_table_a (
  id varchar(20) PRIMARY KEY,
  name varchar NOT NULL,
  value varchar NOT NULL,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone NOT NULL
);

CREATE TABLE dummy_table_b (
  id varchar(20) PRIMARY KEY,
  name varchar NOT NULL,
  value varchar NOT NULL,
  attribute_a varchar,
  attribute_b integer,
  created_at timestamp with time zone NOT NULL,
  updated_at timestamp with time zone NOT NULL
);
